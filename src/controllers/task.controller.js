const request = require('request');
const appConfig = require('../config/app.config');
const db = require('../models');
const Task = db.tasks;

exports.getAll = (req, res, next) => {
    Task.findAll()
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message
            });
        });
};

exports.getById = (req, res, next) => {
    const id = req.params.id;

    Task.findByPk(id)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message
            });
        });
};

exports.create = (req, res, next) => {
    const task = {
        responsibleName: req.body.responsibleName,
        responsibleEmail: req.body.responsibleEmail,
        description: req.body.description
    }

    request({
        url: 'http://apilayer.net/api/check?access_key=' + appConfig.apilayerToken + '&email=' + task.responsibleEmail,
        method: 'GET'
    }, function (err, response, body) {
        const result = JSON.parse(body);

        if (result.mx_found && result.format_valid) {
            Task.create(task)
                .then((data) => {
                    res.status(201).send(data);
                })
                .catch((err) => {
                    res.status(400).send({
                        message: err.message
                    });
                });

            return;
        }

        res.status(400).send({message: 'E-mail inválido, você quis dizer: ' + result.did_you_mean});
    });
};

exports.random = async (req, res, next) => {

}

exports.update = async (req, res, next) => {
    const id = req.params.id;

    const task = await Task.findByPk(id);
    if (!task) {
        res.send('Task not found');
    }

    let body = req.body;
    if (body.completed === false) {
        if (task.reversed === 2) {
            return res.status(400).send({message: 'Reversed limit reached!'});
        }

        if (!body.supervisorPassword) {
            return res.status(400).send({message: 'Send the password of supervisor!'});
        }

        if (body.supervisorPassword !== 'TrabalheNaSaipos') {
            return res.status(400).send({message: 'Supervisor password do not match!'});
        }

        body.reversed = (task.reversed + 1);
    }

    Task.update(body, {
        where: {id}
    })
        .then((updated) => {
            if (updated) {
                Task.findByPk(id)
                    .then((data) => {
                        res.send(data);
                    })
            } else {
                res.status(400).send({message: 'Cannot updated task, check body content'});
            }
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message
            });
        });
};

exports.destroy = (req, res, next) => {
    const id = req.params.id;

    Task.destroy({
        where: {id}
    })
        .then((deleted) => {
            if (deleted) {
                res.send({message: 'Task was deleted.'})
            } else {
                res.status(400).send({message: 'Task was deleted.'})
            }
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message
            });
        });
};
