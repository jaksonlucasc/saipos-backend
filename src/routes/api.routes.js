const TaskController = require('../controllers/task.controller');

module.exports = (app) => {
    app.get('/tasks', TaskController.getAll);
    app.get('/tasks/:id', TaskController.getById);
    app.post('/tasks', TaskController.create);
    app.post('/tasks/random', TaskController.random);
    app.patch('/tasks/:id', TaskController.update);
    app.delete('/tasks/:id', TaskController.destroy);
}
