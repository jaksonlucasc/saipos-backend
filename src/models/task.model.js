module.exports = (sequelize, Sequelize) => {
    return sequelize.define('tasks', {
        responsibleName: {
            type: Sequelize.STRING
        },
        responsibleEmail: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        completed: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        reversed: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    });
}
