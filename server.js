const express = require('express');
const cors = require('cors');
const app = express();
const db = require('./src/models');

// Set cors
app.use(cors());

// Parse requests of json
app.use(express.json());

// Initialize db
db.sequelize.sync();

// Load routes
require('./src/routes/api.routes')(app);

// Run
app.listen(8080, () => {
    console.log(`Server is running.`);
});
